#Solr and Elastic Geo search speed test

##GOAL.


Test both solr and elastic technologies speed dealing with geo search.

TESTING SOLR AND ELASTIC SERVERS.

-Solr version 5.5
-Elastic version 1.5.2

-Starting Solr as 
  ./solr start -e techproducts
-Staring elastic as
  ./elastic
  
##RESULTS.
  
We searched for a specified latitude and longitude within 1km.

- Solr Allocated memory: 512mb

- Solr time result: 
* First time is using postman. 
* Second time correspond to a search through spring data. 
* The third time correspond to the first query done after inserting all rows in one shot. This time is irrelevant as any other search following will perform as the second time registered. 
 
|records    	|    Solr  time/results  (ms) 	|   Elastic time/results(ms)|
|---------------|:-----------------------------:|:-------------------------:|
|500,000     	|         37-56/5556           	|	    11/5556				|
|1,000,000   	|         30-68-120/11112		|		45/11112			|
|1,500,000   	|         35-68-143/16667		|		35/16667			|
|2,000,000   	|         32-31-86/22223		|		41/22223			|
|2,500,000   	|         31-78-117/27778		|		45/27778			|
|3,000,000   	|         35-35-142/33334		|		54/33334			|
|3,500,000   	|         40-58-278/38889		|		130/38889			|
|4,000,000   	|         45-46-345/44445		|		130/44445			|
|4,500,000   	|         53-63-660/50000		|		75/50000			|
|5,000,000   	|         70-42-674/55556		|		180/55556			|
|5,500,000   	|         77-66-640/61112		|		230/61112			|
|6,000,000   	|         68-47-580/66667		|		220/66667			|
|6,500,000   	|         75-72-655/72223		|		270/72223			|
|7,000,000   	|         90-50-630/77778		|		300/77778			|
|7,500,000   	|         80-56-1112/83334		|		java heap space		|
|8,000,000   	|         100-54-1203/88889		|		java heap space		|
|8,500,000   	|         95-119-1240/94445		|		java heap space		|
|9,000,000   	|         94-60-1114/100000		|		java heap space		|
|9,500,000     	|         100-85-1269/105556    |		java heap space		|
|10,000,000   	|         110-67-1391/111112	|		java heap space		|
|10,500,000  	|         115-99-1473/116667	|		java heap space		|
|11,000,000 	|         134-140-1459/122223	|		java heap space		|
|11,500,000		|         137-132-1345/127778	|		java heap space		|
|12,000,000		|         135-147-1445/127778	|		java heap space		|

##SPRING DATA and ELASTIC integration issues

Elastic search spring data integration in geo search does not work properly. The build query is:

_{"query" : {"match_all" : { }},"post_filter" : {"geo_distance" : {"location" : [ 46.18814, -93.88541 ],"distance" : "100km"}}}_

However, the one that really search within the circle has the following structure: 

_{"query": {"filtered": {"query": {"match_all": {}},"filter": {"geo_distance": {"distance": "100km","location": {"lat": 46.884106,"lon": -93.377042}}}}}}_

*Actually, there are many query which are not being understood by the server.

*Moreover, the elastic version server had to be 1.5.2  and the update to use the last one is really hard to do.

#Tried to get more performance.

After configuring elastic in yml config:
index.store.type: niofs
index.translog.sync_interval: 30s
index.translog.durability: async
index.translog.fs.type: buffered
index.translog.flush_threshold_ops: 100000
threadpool.bulk.queue_size: 900
indices.fielddata.cache.size: 12%
bootstrap.mlockall: false

**still out of memory issues came up when inserting rows**
**So the second bit of results for elastic I did used postman to run the query.**
**Apart from that to save all 12 million rows I had to use a Java heap space of 2g. Many times I got JAVA HEAD SPACE exception.**

##Solr query example run on postman.
__http://localhost:8983/solr/techproducts/select?q=*:*&fq={!geofilt%20pt=23.19814,-93.88541%20sfield=store%20d=10}__