package ca.tech.solrvselastic.config;

import java.net.InetAddress;

import org.elasticsearch.client.Client;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.transport.TransportAddress;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.TransportClientFactoryBean;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@Configuration
@EnableElasticsearchRepositories(basePackages = { "ca.tech.solrvselastic.product.elastic" })
public class SearchElasticContext {

	private @Value("${esearch.host}") String hostname;
	private @Value("${esearch.port}") int port;



	@Bean
	public ElasticsearchOperations elasticsearchTemplate() throws Exception {
		return new ElasticsearchTemplate(client());
	}

	@Bean
	public Client client() throws Exception {

		TransportClientFactoryBean client = new TransportClientFactoryBean();
		client.setClusterNodes("localhost:9300"); 
		client.setClusterName("elasticsearch");
		client.afterPropertiesSet();
		return client.getObject();
	}

}
