package ca.tech.solrvselastic.product.elastic;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;
import org.springframework.data.elasticsearch.core.query.Criteria;
import org.springframework.data.elasticsearch.core.query.CriteriaQuery;
import org.springframework.stereotype.Service;

import ca.tech.solrvselastic.product.model.ProductElastic;

@Service
public class ProductElasticDAO {

	@Resource
	private ProductElasticRepository productRepository;
	
	@Autowired
    private ElasticsearchTemplate elasticsearchTemplate;
	
	public List<ProductElastic> findByLocation(double latitude, double longitude, int distance){
		CriteriaQuery geoLocationCriteriaQuery = new CriteriaQuery(
                new Criteria("location").within(new GeoPoint(longitude,latitude), distance + "km"));
		//Long a = this.elasticsearchTemplate.count(geoLocationCriteriaQuery);
		return this.elasticsearchTemplate.queryForList(geoLocationCriteriaQuery, ProductElastic.class);
	}

	public ProductElasticRepository getProductRepository() {
		return productRepository;
	}

	public void setProductRepository(ProductElasticRepository productRepository) {
		this.productRepository = productRepository;
	}
	
}
