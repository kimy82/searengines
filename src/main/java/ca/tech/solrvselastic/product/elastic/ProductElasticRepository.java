package ca.tech.solrvselastic.product.elastic;

import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;

import ca.tech.solrvselastic.product.model.ProductElastic;

public interface ProductElasticRepository extends ElasticsearchCrudRepository<ProductElastic,String> {

}
