package ca.tech.solrvselastic.product.solr;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.geo.Distance;
import org.springframework.data.solr.core.geo.Point;
import org.springframework.data.solr.repository.SolrCrudRepository;

import ca.tech.solrvselastic.product.model.ProductSolr;

public interface ProductSolrRepository extends SolrCrudRepository<ProductSolr, String> {

	Page<ProductSolr> findByLocationWithin(Point location, Distance distance,
			Pageable pagebale);

}
