package ca.tech.solrvselastic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import ca.tech.solrvselastic.config.SearchElasticContext;
import ca.tech.solrvselastic.config.SearchSolrContext;

@Configuration
@ComponentScan
@EnableAutoConfiguration
@Import({ SearchSolrContext.class , SearchElasticContext.class})
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
