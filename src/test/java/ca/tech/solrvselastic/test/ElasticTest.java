package ca.tech.solrvselastic.test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import ca.tech.solrvselastic.Application;
import ca.tech.solrvselastic.product.elastic.ProductElasticDAO;
import ca.tech.solrvselastic.product.model.ProductElastic;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = { Application.class })
public class ElasticTest {

	@Resource
	private ProductElasticDAO productElasticDAO;

	private Long index =0L;

	@Before
	public void setUp() {
		//this.productElasticDAO.getProductRepository().deleteAll();
		addBatch(7000000L);
	}

	@Test
	public void ProductElasticGeoSpeedTest() {
		for (int numberOfBatch = 0; numberOfBatch < 14; numberOfBatch++) {
			Calendar cal = Calendar.getInstance();
			Long startMil = cal.getTimeInMillis();

			List<ProductElastic> products = this.productElasticDAO
					.findByLocation(45.18814, -93.88541, 1);

			System.out.println("Total products: " + products.size());
			cal = Calendar.getInstance();
			Long endMil = cal.getTimeInMillis();

			System.out.println("Time spent on " + index + " is " + (endMil - startMil) + "ms");
			addBatch(500000L);
		}
	}
	

	@Test
	public void sProductElasticGeoSpeedTest() {
			Calendar cal = Calendar.getInstance();
			Long startMil = cal.getTimeInMillis();

			System.out.println( this.productElasticDAO.getProductRepository().count());
			List<ProductElastic> products = this.productElasticDAO
					.findByLocation(46.18814, -93.88541, 100000);
			System.out.println("Total products: " + products.size());
			
			cal = Calendar.getInstance();
			Long endMil = cal.getTimeInMillis();

			System.out.println("Time spent on " + index + " is " + (endMil - startMil) + "ms");
	}

	private void addBatch(Long numberOfInserts) {

		List<ProductElastic> list = new ArrayList<ProductElastic>();
		Long lastIndex = index;
		for (index = lastIndex; index < (numberOfInserts + lastIndex); index++) {
			ProductElastic product = new ProductElastic();
			product.setId(String.valueOf(index));
			product.setAvailable(true);
			double latitude = 46.18814;
			latitude = (latitude + index) % 90;
			product.setLocation(new GeoPoint(latitude, -93.88541));
			product.setName("name" + index);
			product.setPrice(2.3f);
			list.add(product);
			if (list.size() > 100000) {
				saveList(list);
			}
		}

		saveList(list);
	}

	private void saveList(List<ProductElastic> list) {
		this.productElasticDAO.getProductRepository().save(list);
		System.out.println("Saving>> " + index);
		list.clear();
	}
}
