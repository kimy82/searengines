package ca.tech.solrvselastic.test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.geo.Distance;
import org.springframework.data.solr.core.geo.Point;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import ca.tech.solrvselastic.Application;
import ca.tech.solrvselastic.product.model.ProductSolr;
import ca.tech.solrvselastic.product.solr.ProductSolrRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = { Application.class })
public class SolrTest {

	@Resource
	private ProductSolrRepository productRepository;

	private Long index = 0L;

	@Before
	public void setUp() {
		this.index = this.productRepository.count();
		//this.productRepository.deleteAll();
		addBatch(500000L);
	}

	@Test
	public void ProductSolrGeoTest() {

		for (int numberOfBatch = 0; numberOfBatch < 25; numberOfBatch++) {
			
			double latitude = 0.18814;
			latitude = ((int)(Math.random()*90)) + latitude;
			
			
			Calendar cal = Calendar.getInstance();
			Long startMil = cal.getTimeInMillis();

			
			Point geoPoint = new Point(latitude, -93.88541);
			Distance distance = new Distance(1);
			Pageable page = new PageRequest(0, 20);
			Page<ProductSolr> productsPage = this.productRepository.findByLocationWithin(geoPoint, distance, page);

			System.out.println("Total products"	+ productsPage.getTotalElements());
			cal = Calendar.getInstance();
			Long endMil = cal.getTimeInMillis();

			System.out.println("Time spent on " + index + ": "	+ (endMil - startMil));
			
			addBatch(500000L);
		}
	}
	
	@Test
	public void ProductSolrGeoTestO() {

			Calendar cal = Calendar.getInstance();
			Long startMil = cal.getTimeInMillis();

			Point geoPoint = new Point(29.18814, -93.88541);
			Distance distance = new Distance(1);
			Pageable page = new PageRequest(0, 20);
			Page<ProductSolr> productsPage = this.productRepository.findByLocationWithin(geoPoint, distance, page);

			System.out.println("Total products"	+ productsPage.getTotalElements());
			cal = Calendar.getInstance();
			Long endMil = cal.getTimeInMillis();

			System.out.println("Time spent on " + index + ": "	+ (endMil - startMil));
	}

	private void addBatch(Long numberOfInserts) {

		List<ProductSolr> list = new ArrayList<ProductSolr>();
		Long lastIndex = index;
		for (index = lastIndex; index < (numberOfInserts + lastIndex); index++) {
			ProductSolr product = new ProductSolr();
			product.setId(String.valueOf(index));
			product.setAvailable(true);
			double latitude = 45.18814;
			latitude = (latitude + index) % 90;
			product.setLocation(latitude + ", -93.88541");
			product.setName("name" + index);
			product.setPrice(2.3f);
			list.add(product);
			if (list.size() > 100000) {
				saveList(list);
			}
		}

		saveList(list);
	}

	private void saveList(List<ProductSolr> list) {
		this.productRepository.save(list);
		System.out.println("Saving>> " + index);
		list.clear();
	}
}
